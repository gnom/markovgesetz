# MarkovGesetz

## Configuration

`MARKOV_CORPUS_PATH` = path to a text file containing the corpus

`MARKOV_BASE_URL` = base url of the mastodon instance e.g. https://social.example.com

`MARKOV_ACCESS_TOKEN` = accesstoken of the mastodon account

`MARKOV_LAW_MAX_TRIES` = maximum number of tries to find a law shorter then MARKOV_LAW_MAX_LENGTH

`MARKOV_LAW_MAX_TRIES` = maximum length of a generated law

`MARKOV_HASHTAGS` = comma separated list of hashtags eg. foo,bar,baz the `#` are added automatically.