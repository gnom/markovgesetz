use futures::executor::block_on;
use markov::Chain;
use megalodon::{entities, error, generator, response::Response, SNS};
use tokio;

struct Config {
    corpus_path: String,
    base_url: String,
    access_token: String,
    law_max_length: usize,
    law_max_tries: u8,
    hashtags: Vec<String>,
}

#[tokio::main]
async fn main() {
    let config = load_config();
    let law = generate_law(&config).expect("No short enough law generated in 10 tries.");
    match post_to_mastodon(&config, &law) {
        Ok(_) => println!("Toot send."),
        Err(e) => panic!("{}", e),
    }
}

fn generate_law(config: &Config) -> Result<String, String> {
    let mut chain = Chain::of_order(2);
    chain
        .feed_file(config.corpus_path.clone())
        .expect(format!("Unable to load corpus from path {}", config.corpus_path).as_str());
    for _ in 0..config.law_max_tries {
        let law = chain.generate_str();
        if law.len() <= config.law_max_length {
            return Ok(law);
        }
    }
    return Err(String::from("No short enough law found."));
}

fn post_to_mastodon(
    config: &Config,
    law: &String,
) -> Result<Response<entities::Status>, error::Error> {
    let client = generator(
        SNS::Mastodon,
        config.base_url.clone(),
        Some(config.access_token.clone()),
        None,
    );
    block_on(client.post_status(
        format!("{}\n\n{}", law.to_string(), config.hashtags.join(" ")),
        None,
    ))
}

fn load_config() -> Config {
    let corpus_path = std::env::var("MARKOV_CORPUS_PATH").expect("MARKOV_CORPUS_PATH must be set.");
    let base_url = std::env::var("MARKOV_BASE_URL").expect("MARKOV_BASE_URL must be set.");
    let access_token =
        std::env::var("MARKOV_ACCESS_TOKEN").expect("MARKOV_ACCESS_TOKEN must be set.");
    let law_max_tries: u8 = match std::env::var("MARKOV_LAW_MAX_TRIES") {
        Err(_) => 10,
        Ok(i) => i.parse().expect("value in range 1 - 255 expected."),
    };
    let law_max_length: usize = match std::env::var("MARKOV_LAW_MAX_LENGTH") {
        Err(_) => 300,
        Ok(i) => i.parse().expect("positive integer expected."),
    };
    let hashtags: Vec<String> = match std::env::var("MARKOV_HASHTAGS") {
        Err(_) => vec![],
        Ok(s) => s
            .split(',')
            .map(|t| format!("#{}", t.to_string()))
            .collect(),
    };
    let config = Config {
        corpus_path,
        base_url,
        access_token,
        law_max_length,
        law_max_tries,
        hashtags,
    };
    return config;
}
